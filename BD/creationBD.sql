-- DROP
DROP TABLE IF EXISTS UTILISATEUR_DETIENT_BILLET;
DROP TABLE IF EXISTS MEMBRES_GROUPES;
DROP TABLE IF EXISTS LIENS_RESEAUX;
DROP TABLE IF EXISTS LIENS_VIDEOS;
DROP TABLE IF EXISTS PHOTOS;
DROP TABLE IF EXISTS FAVORIS_SPECTATEURS;
DROP TABLE IF EXISTS HEBERGEMENT_GROUPES;
DROP TABLE IF EXISTS ACTIVITES_ANNEXES;
DROP TABLE IF EXISTS PANIER_BILLETS;
DROP TABLE IF EXISTS PANIER;
DROP TABLE IF EXISTS UTILISATEURS;
DROP TABLE IF EXISTS BILLETS;
DROP TABLE IF EXISTS CONCERTS;
DROP TABLE IF EXISTS ARTISTES;
DROP TABLE IF EXISTS GROUPES;
DROP TABLE IF EXISTS STYLEMUSICAL;
DROP TABLE IF EXISTS LIEUX;

-- Table LIEUX
CREATE TABLE LIEUX(
    lieu_id INT PRIMARY KEY,
    nomLieu VARCHAR(50) NOT NULL,
    adresse VARCHAR(50),
    capacite_max INT
);

-- Table GROUPES
CREATE TABLE GROUPES (
    groupe_id INT PRIMARY KEY,
    nomGroupe VARCHAR(50) NOT NULL,
    desc_groupe TEXT,
    style_musical_id INT,
    date_heure_arrivee DATETIME NOT NULL,
    date_heure_depart DATETIME NOT NULL
);

CREATE TABLE LIENS_RESEAUX(
    lien VARCHAR(100) PRIMARY KEY,
    groupe_id INT,
    artiste_id INT
);

CREATE TABLE LIENS_VIDEOS(
    lien VARCHAR(100) PRIMARY KEY,
    groupe_id INT,
    artiste_id INT
);

CREATE TABLE PHOTOS(
    photo_id INT PRIMARY KEY,
    lien VARCHAR(100),
    groupe_id INT,
    artiste_id INT
);

-- Table ARTISTES
CREATE TABLE ARTISTES (
    artiste_id INT PRIMARY KEY,
    nomArtiste VARCHAR(25) NOT NULL,
    desc_artiste TEXT,
    categorie TEXT,
    style_musical_id INT,
    date_heure_arrivee DATETIME,
    date_heure_depart DATETIME,
    instrument_joue VARCHAR(50),
    image_data LONGBLOB
);

CREATE TABLE STYLEMUSICAL(
    style_musical_id INT PRIMARY KEY,
    nomStyle VARCHAR(25) NOT NULL
);

-- Table CONCERTS
CREATE TABLE CONCERTS (
    concert_id INT PRIMARY KEY,
    groupe_id INT,
    artiste_id INT,
    lieu_id INT,
    date_heure_debut DATETIME NOT NULL,
    date_heure_fin DATETIME NOT NULL,
    duree_montage INT NOT NULL,
    duree_demontage INT NOT NULL,
    preinscription_requise BOOLEAN default TRUE,
    gratuit BOOLEAN default FALSE,
    places_disponibles INT
);

-- Table BILLETS
CREATE TABLE BILLETS (
    billet_id INT PRIMARY KEY,
    typeBillet VARCHAR(50) NOT NULL,
    prix DECIMAL(10, 2) NOT NULL,
    quantite_disponible INT,
    lieu_id INT
);

-- Table ACTIVITES_ANNEXES
CREATE TABLE ACTIVITES_ANNEXES (
    activite_id INT PRIMARY KEY,
    nom_activite VARCHAR(50) NOT NULL,
    groupe_id INT,
    artiste_id INT,
    lieu_id INT,
    date_heure_debut DATETIME,
    date_heure_fin DATETIME,
    desc_activite TEXT
);

-- Table FAVORIS_SPECTATEURS
CREATE TABLE FAVORIS_SPECTATEURS (
    favori_id INT PRIMARY KEY,
    utilisateur_id INT,
    groupe_id INT,
    artiste_id INT
);

CREATE TABLE UTILISATEURS(
    utilisateur_id INT PRIMARY KEY,
    billet_id INT,
    nomUser VARCHAR(25),
    prenomUser VARCHAR(25),
    email VARCHAR(50),
    mot_de_passe VARCHAR(100),
    date_naissance DATE
);

-- Table PANIER
CREATE TABLE PANIER (
    panier_id INT PRIMARY KEY,
    utilisateur_id INT,
    FOREIGN KEY (utilisateur_id) REFERENCES UTILISATEURS(utilisateur_id)
);

-- Table PANIER_BILLETS (pour la relation entre panier et billets)
CREATE TABLE PANIER_BILLETS (
    panier_id INT,
    billet_id INT,
    quantite_achetee INT,
    PRIMARY KEY (panier_id, billet_id),
    FOREIGN KEY (panier_id) REFERENCES PANIER(panier_id),
    FOREIGN KEY (billet_id) REFERENCES BILLETS(billet_id)
);


-- Table HEBERGEMENT_GROUPES
CREATE TABLE HEBERGEMENT_GROUPES (
    hebergement_id INT PRIMARY KEY,
    groupe_id INT,
    date_arrivee DATE,
    date_depart DATE,
    nombre_places INT
);

CREATE TABLE UTILISATEUR_DETIENT_BILLET(
    utilisateur_id INT,
    billet_id INT,
    PRIMARY KEY (utilisateur_id, billet_id)
);

CREATE TABLE MEMBRES_GROUPES(
    artiste_id INT,
    groupe_id INT,
    PRIMARY KEY (artiste_id, groupe_id)
);

-- CLES ETRANGERES

ALTER TABLE GROUPES ADD FOREIGN KEY (style_musical_id) REFERENCES STYLEMUSICAL(style_musical_id);

ALTER TABLE ARTISTES ADD FOREIGN KEY (style_musical_id) REFERENCES STYLEMUSICAL(style_musical_id);

ALTER TABLE CONCERTS ADD FOREIGN KEY (groupe_id) REFERENCES GROUPES(groupe_id);
ALTER TABLE CONCERTS ADD FOREIGN KEY (artiste_id) REFERENCES ARTISTES(artiste_id);
ALTER TABLE CONCERTS ADD FOREIGN KEY (lieu_id) REFERENCES LIEUX(lieu_id);

ALTER TABLE BILLETS ADD FOREIGN KEY (lieu_id) REFERENCES LIEUX(lieu_id);

ALTER TABLE ACTIVITES_ANNEXES ADD FOREIGN KEY (groupe_id) REFERENCES GROUPES(groupe_id);
ALTER TABLE ACTIVITES_ANNEXES ADD FOREIGN KEY (lieu_id) REFERENCES LIEUX(lieu_id);

ALTER TABLE FAVORIS_SPECTATEURS ADD FOREIGN KEY (utilisateur_id) REFERENCES UTILISATEURS(utilisateur_id);
ALTER TABLE FAVORIS_SPECTATEURS ADD FOREIGN KEY (groupe_id) REFERENCES GROUPES(groupe_id);
ALTER TABLE FAVORIS_SPECTATEURS ADD FOREIGN KEY (artiste_id) REFERENCES ARTISTES(artiste_id);

ALTER TABLE HEBERGEMENT_GROUPES ADD FOREIGN KEY (groupe_id) REFERENCES GROUPES(groupe_id);

ALTER TABLE UTILISATEUR_DETIENT_BILLET ADD FOREIGN KEY (billet_id) REFERENCES BILLETS(billet_id);
ALTER TABLE UTILISATEUR_DETIENT_BILLET ADD FOREIGN KEY (utilisateur_id) REFERENCES UTILISATEURS(utilisateur_id);

ALTER TABLE MEMBRES_GROUPES ADD FOREIGN KEY (artiste_id) REFERENCES ARTISTES(artiste_id);
ALTER TABLE MEMBRES_GROUPES ADD FOREIGN KEY (groupe_id) REFERENCES GROUPES(groupe_id);

ALTER TABLE LIENS_RESEAUX ADD FOREIGN KEY (groupe_id) REFERENCES GROUPES(groupe_id);
ALTER TABLE LIENS_RESEAUX ADD FOREIGN KEY (artiste_id) REFERENCES ARTISTES(artiste_id);

ALTER TABLE LIENS_VIDEOS ADD FOREIGN KEY (groupe_id) REFERENCES GROUPES(groupe_id);
ALTER TABLE LIENS_VIDEOS ADD FOREIGN KEY (artiste_id) REFERENCES ARTISTES(artiste_id);

ALTER TABLE PHOTOS ADD FOREIGN KEY (groupe_id) REFERENCES GROUPES(groupe_id);
ALTER TABLE PHOTOS ADD FOREIGN KEY (artiste_id) REFERENCES ARTISTES(artiste_id);


-- TRIGGERS

delimiter |
CREATE TRIGGER check_lieu_concert_occupe
BEFORE INSERT ON CONCERTS FOR EACH ROW
BEGIN
    IF EXISTS (
        SELECT 1 FROM CONCERTS
        WHERE lieu_id = NEW.lieu_id
        AND (
            (NEW.date_heure_debut BETWEEN date_heure_debut AND date_heure_fin + INTERVAL duree_demontage MINUTE)
            OR (NEW.date_heure_fin + INTERVAL duree_montage MINUTE BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_debut <= date_heure_debut AND NEW.date_heure_fin + INTERVAL duree_demontage MINUTE >= date_heure_fin + INTERVAL duree_montage MINUTE)
        )
    ) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Un autre concert a lieu au même endroit pendant cette période.';
    END IF;
END |
delimiter ;


delimiter |
CREATE TRIGGER check_groupe_occupe_concert
BEFORE INSERT ON CONCERTS FOR EACH ROW
BEGIN
    IF EXISTS (
        SELECT 1 FROM CONCERTS
        WHERE groupe_id = NEW.groupe_id
        AND (
            (NEW.date_heure_debut BETWEEN date_heure_debut AND date_heure_fin + INTERVAL duree_demontage MINUTE)
            OR (NEW.date_heure_fin + INTERVAL duree_montage MINUTE BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_debut <= date_heure_debut AND NEW.date_heure_fin + INTERVAL duree_demontage MINUTE >= date_heure_fin + INTERVAL duree_montage MINUTE)
        )
    ) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le groupe a déjà un autre concert pendant cette période.';
    END IF;
END |
delimiter ;

delimiter |
CREATE TRIGGER check_groupe_occupe_in_activite_concert
BEFORE INSERT ON CONCERTS FOR EACH ROW
BEGIN
    IF EXISTS (
        SELECT 1 FROM ACTIVITES_ANNEXES
        WHERE groupe_id = NEW.groupe_id
        AND (
            (NEW.date_heure_debut BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_fin BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_debut <= date_heure_debut AND NEW.date_heure_fin >= date_heure_fin)
        )
    ) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le groupe participe déjà à une activité annexe pendant cette période.';
    END IF;
END |
delimiter ;

delimiter |
CREATE TRIGGER check_artiste_occupe_concert
BEFORE INSERT ON CONCERTS FOR EACH ROW
BEGIN
    IF EXISTS (
        SELECT 1 FROM CONCERTS
        WHERE artiste_id = NEW.artiste_id
        AND (
            (NEW.date_heure_debut BETWEEN date_heure_debut AND date_heure_fin + INTERVAL duree_demontage MINUTE)
            OR (NEW.date_heure_fin + INTERVAL duree_montage MINUTE BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_debut <= date_heure_debut AND NEW.date_heure_fin + INTERVAL duree_demontage MINUTE >= date_heure_fin + INTERVAL duree_montage MINUTE)
        )
    ) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "L'artiste a déjà un autre concert pendant cette période.";
    END IF; 
END |
delimiter ;

delimiter |
CREATE TRIGGER check_artiste_occupe_in_activite_concert
BEFORE INSERT ON CONCERTS FOR EACH ROW
BEGIN
    IF EXISTS (
        SELECT 1 FROM ACTIVITES_ANNEXES
        WHERE artiste_id = NEW.artiste_id
        AND (
            (NEW.date_heure_debut BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_fin BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_debut <= date_heure_debut AND NEW.date_heure_fin >= date_heure_fin)
        )
    ) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "L'artiste participe déjà à une activité annexe pendant cette période.";
    END IF;
END |
delimiter ;

delimiter |
CREATE TRIGGER check_artiste_or_groupe_concert
BEFORE INSERT ON CONCERTS FOR EACH ROW
BEGIN
    IF NEW.artiste_id IS NOT NULL AND NEW.groupe_id IS NOT NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Un concert ne peut pas avoir à la fois un groupe et un artiste.";
    END IF;
    IF NEW.artiste_id IS NULL AND NEW.groupe_id IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Un concert avoir obligatoirement soit un groupe, soit un artiste.";
    END IF;
END |
delimiter ;


delimiter |
CREATE TRIGGER check_groupe_occupe__in_concert_activite
BEFORE INSERT ON ACTIVITES_ANNEXES FOR EACH ROW
BEGIN
    IF EXISTS (
        SELECT 1 FROM CONCERTS
        WHERE groupe_id = NEW.groupe_id
        AND (
            (NEW.date_heure_debut BETWEEN date_heure_debut AND date_heure_fin + INTERVAL duree_demontage MINUTE)
            OR (NEW.date_heure_fin + INTERVAL duree_montage MINUTE BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_debut <= date_heure_debut AND NEW.date_heure_fin + INTERVAL duree_demontage MINUTE >= date_heure_fin + INTERVAL duree_montage MINUTE)
        )
    ) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le groupe a déjà un concert pendant cette période.';
    END IF;
END |
delimiter ;

delimiter |
CREATE TRIGGER check_groupe_occupe_activite
BEFORE INSERT ON ACTIVITES_ANNEXES FOR EACH ROW
BEGIN
    IF EXISTS (
        SELECT 1 FROM ACTIVITES_ANNEXES
        WHERE groupe_id = NEW.groupe_id
        AND (
            (NEW.date_heure_debut BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_fin BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_debut <= date_heure_debut AND NEW.date_heure_fin >= date_heure_fin)
        )
    ) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Le groupe participe déjà à une activité annexe pendant cette période.';
    END IF;
END |
delimiter ;

delimiter |
CREATE TRIGGER check_artiste_occupe_in_concert_activite
BEFORE INSERT ON ACTIVITES_ANNEXES FOR EACH ROW
BEGIN
    IF EXISTS (
        SELECT 1 FROM CONCERTS
        WHERE artiste_id = NEW.artiste_id
        AND (
            (NEW.date_heure_debut BETWEEN date_heure_debut AND date_heure_fin + INTERVAL duree_demontage MINUTE)
            OR (NEW.date_heure_fin + INTERVAL duree_montage MINUTE BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_debut <= date_heure_debut AND NEW.date_heure_fin + INTERVAL duree_demontage MINUTE >= date_heure_fin + INTERVAL duree_montage MINUTE)
        )
    ) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "L'artiste a déjà un concert pendant cette période.";
    END IF;
END |
delimiter ;

delimiter |
CREATE TRIGGER check_artiste_occupe_activite
BEFORE INSERT ON ACTIVITES_ANNEXES FOR EACH ROW
BEGIN
    IF EXISTS (
        SELECT 1 FROM ACTIVITES_ANNEXES
        WHERE artiste_id = NEW.artiste_id
        AND (
            (NEW.date_heure_debut BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_fin BETWEEN date_heure_debut AND date_heure_fin)
            OR (NEW.date_heure_debut <= date_heure_debut AND NEW.date_heure_fin >= date_heure_fin)
        )
    ) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "L'artiste participe déjà à une activité annexe pendant cette période.";
    END IF;
END |
delimiter ;