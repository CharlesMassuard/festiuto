-- Inserting data into LIEUX table
INSERT INTO LIEUX (lieu_id, nomLieu, adresse, capacite_max) VALUES
(1, 'Grande Scène', 'Parc des Morinières, 85470 Bretignolles-sur-Mer', 12000),
(2, 'Petite Scène', 'Parc des Morinières, 85470 Bretignolles-sur-Mer', 7500),
(3, 'Dédicades Center', 'Parc des Morinières, 85470 Bretignolles-sur-Mer', 50);

-- Inserting data into STYLEMUSICAL table
INSERT INTO STYLEMUSICAL (style_musical_id, nomStyle) VALUES
(1, 'Pop Rock'),
(2, 'Rock Alternatif'),
(3, 'Pop'),
(4, 'Soul'),
(5, 'Funk Rock'),
(6, 'Punk Harcore');

-- Inserting data into GROUPES table
INSERT INTO GROUPES (groupe_id, nomGroupe, desc_groupe, style_musical_id, date_heure_arrivee, date_heure_depart) VALUES
(1, 'Coldplay', 'Coldplay est un groupe de pop rock britannique.', 1, '2023-06-01 10:00:00', '2023-06-02 12:00:00'),
(2, "Red Hot Chili Peppers", "Groupe de funck rock américain", 5, '2023-07-17 15:00:00', '2023-07-18 10:00:00'),
(3, "Tagada Jones", "Groupe de punk français, pour vous servir", 6, '2023-07-18 18:00:00', '2023-07-19 03:00:00');

-- Inserting data into ARTISTES table
INSERT INTO ARTISTES (artiste_id, nomArtiste, desc_artiste,categorie ,style_musical_id, date_heure_arrivee, date_heure_depart, instrument_joue,image_data) VALUES
(1, 'Ed Sheeran', 'Ed Sheeran est un auteur-compositeur-interprète et guitariste britannique.','Chanteur' ,3, '2023-06-01 10:00:00', '2023-06-02 12:00:00', 'Guitare',NULL),
(2, 'Adele', 'Adele est une chanteuse britannique.', 'Chanteur' ,4, '2023-06-03 11:00:00', '2023-06-04 13:00:00', 'Voix',NULL),
(3, 'Anthony Kiedis', 'Chanteur des Red Hot Chili Peppers','Chanteur' , 5, '2023-07-17 15:00:00', '2023-07-18 10:00:00', 'Voix',NULL),
(4, 'Flea', 'Bassiste des Red Hot Chili Peppers','Chanteur' , 5, '2023-07-17 15:00:00', '2023-07-18 10:00:00', 'Basse',NULL),
(5, 'John Frusciante', 'Guitariste des Red Hot Chili Peppers', 'Chanteur' ,5, '2023-07-17 15:00:00', '2023-07-18 10:00:00', 'Guitare',NULL),
(6, 'Chad Smith', 'Batteur des Red Hot Chili Peppers', 'Chanteur' ,5, '2023-07-17 15:00:00', '2023-07-18 10:00:00', 'Batterie',NULL),
(7, 'Niko', 'Guitariste de Tagada Jones', 'Chanteur' ,6, '2023-07-18 18:00:00', '2023-07-19 03:00:00', 'Voix et Guitare',NULL),
(8, 'La Guiche', 'Bassiste de Tagada Jones', 'Chanteur' ,6, '2023-07-18 18:00:00', '2023-07-19 03:00:00', 'Guitare',NULL),
(9, 'Waner', 'Batteur de Tagada Jones', 'Chanteur' ,6, '2023-07-18 18:00:00', '2023-07-19 03:00:00', 'basse',NULL),
(10, 'Job', 'Chanteur de Tagada Jones', 'Chanteur' ,6, '2023-07-18 18:00:00', '2023-07-19 03:00:00', 'Batterie',NULL);




-- Inserting data into LIENS_RESEAUX table
INSERT INTO LIENS_RESEAUX (lien, groupe_id, artiste_id) VALUES
('https://www.facebook.com/coldplay', 1, NULL),
('https://www.facebook.com/muse', 2, NULL),
('https://www.facebook.com/EdSheeranMusic', NULL, 1),
('https://www.facebook.com/adele', NULL, 2);

-- Inserting data into LIENS_VIDEOS table
INSERT INTO LIENS_VIDEOS (lien, groupe_id, artiste_id) VALUES
('https://www.youtube.com/watch?v=Qtb11P1FWnc', 1, NULL),
('https://www.youtube.com/watch?v=Ek0SgwWmF9w', 2, NULL),
('https://www.youtube.com/watch?v=JGwWNGJdvx8', NULL, 1),
('https://www.youtube.com/watch?v=YQHsXMglC9A', NULL, 2);

-- Inserting data into PHOTOS table
INSERT INTO PHOTOS (photo_id, lien, groupe_id, artiste_id) VALUES
(1, 'https://www.coldplay.com/wp-content/uploads/2021/05/Higher-Power-1.jpg', 1, NULL),
(2, 'https://www.muse.mu/sites/muse.mu/files/styles/image_16_9/public/2021-05/Screen%20Shot%202021-05-07%20at%2011.00.00.png?itok=Z3Z3Z3Z3', 2, NULL),
(3, 'https://www.coldplay.com/wp-content/uploads/2021/05/Higher-Power-1.jpg', NULL, 1),
(4, 'https://www.muse.mu/sites/muse.mu/files/styles/image_16_9/public/2021-05/Screen%20Shot%202021-05-07%20at%2011.00.00.png?itok=Z3Z3Z3Z3', NULL, 2);

-- Inserting data into CONCERTS table
INSERT INTO CONCERTS (concert_id, groupe_id, artiste_id, lieu_id, date_heure_debut, date_heure_fin, duree_montage, duree_demontage, preinscription_requise, gratuit, places_disponibles) VALUES
(1, 1, NULL, 1, '2023-06-02 20:00:00', '2023-06-02 23:00:00', 120, 60, TRUE, FALSE, 12000),
(2, NULL, 1, 2, '2023-06-02 20:00:00', '2023-06-02 23:00:00', 120, 60, FALSE, FALSE, 7000),
(3, 2, NULL, 1, '2023-07-17 21:00:00', '2023-07-17 23:00:00', 120, 60, TRUE, FALSE, 12000),
(4, 3, NULL, 1, '2023-07-18 23:45:00', '2023-07-19 01:30:00', 120, 60, TRUE, FALSE, 12000);

-- Inserting data into BILLETS table
INSERT INTO BILLETS (billet_id, typeBillet, prix, quantite_disponible, lieu_id) VALUES
(1, '1 Jour', 50.00, 10000, 1),
(2, '2 Jours', 85.00, 5000, 1),
(3, '1 Jour VIP', 125.00, 1000, 1),
(4, '2 Jours VIP', 200.00, 500, 1),
(5, 'Pass Bénévole', 0.00, 500, 1);

-- Inserting data into ACTIVITES_ANNEXES table
INSERT INTO ACTIVITES_ANNEXES (activite_id, nom_activite, groupe_id, artiste_id, lieu_id, date_heure_debut, date_heure_fin, desc_activite) VALUES
(1, "Dédicace RHCP", 3, NULL, 3, '2023-07-17 18:00:00', '2023-07-17 19:00:00', 'Rencontrez les membres du groupe RHCP pour une séance de dédicaces unique !');

-- Inserting data into UTILISATEURS table
INSERT INTO UTILISATEURS (utilisateur_id, nomUser, prenomUser, email, mot_de_passe, date_naissance) VALUES
(1, 'Dupont', 'Jean', 'jean.dupont@example.com', 'motdepasse123', '1990-01-01'),
(2, 'Martin', 'Marie', 'marie.martin@example.com', 'motdepasse456', '1995-05-05'),
(3, 'Lefebvre', 'Lucie', 'lucie.lefebvre@example.com', 'motdepasse789', '1985-12-15'),
(4, 'Dubois', 'Pierre', 'pierre.dubois@example.com', 'motdepasseabc', '1998-07-20'),
(5, 'Moreau', 'Sophie', 'sophie.moreau@example.com', 'motdepassedef', '1993-03-10'),
(6, 'Girard', 'Julie', 'julie.girard@example.com', 'motdepasse123', '1992-06-12'),
(7, 'Bernard', 'Antoine', 'antoine.bernard@example.com', 'motdepasse456', '1997-09-25'),
(8, 'Petit', 'Camille', 'camille.petit@example.com', 'motdepasse789', '1991-02-28'),
(9, 'Rousseau', 'Lucas', 'lucas.rousseau@example.com', 'motdepasseabc', '1994-11-05'),
(10,'Blanc', 'Manon', 'manon.blanc@example.com', 'motdepassedef', '1988-08-18');

INSERT INTO PANIER (panier_id, utilisateur_id) VALUES 
(1,1),
(2,2),
(3,3),
(4,4),
(5,5),
(6,6),
(7,7),
(8,8),
(9,9),
(10,10);

-- Inserting data into FAVORIS_SPECTATEURS table
INSERT INTO FAVORIS_SPECTATEURS (favori_id, utilisateur_id, groupe_id, artiste_id) VALUES
(1, 1, 1, 2),
(2, 2, 2, NULL);

-- Inserting data into HEBERGEMENT_GROUPES table
INSERT INTO HEBERGEMENT_GROUPES (hebergement_id, groupe_id, date_arrivee, date_depart, nombre_places) VALUES
(1, 1, '2023-06-01', '2023-06-03', 10),
(2, 2, '2023-06-03', '2023-06-05', 8);

INSERT INTO UTILISATEUR_DETIENT_BILLET (utilisateur_id, billet_id) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 1),
(7, 2),
(8, 3),
(9, 4),
(10, 5);

-- Inserting data into MEMBRES_GROUPES table
INSERT INTO MEMBRES_GROUPES(artiste_id, groupe_id) VALUES
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 3),
(8, 3),
(9, 3),
(10, 3);