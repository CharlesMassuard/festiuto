from datetime import datetime

from flask import jsonify, render_template, request

import io
from flask import redirect, render_template, request, session, url_for

from .app import app
from . import models as mo

app.secret_key = 'clé' 

data = [
    {'id': 1, 'nomArtiste': "Yeat", 'categorie': 'Rappeur et chanteur', 'image': '../static/IMG/yeat.webp'},
    {'id': 2, 'nomArtiste': "Drake", 'categorie': 'Rappeur et chanteur', 'image': '../static/IMG/drake.avif'},
    {'id': 3, 'nomArtiste': "Playboi Carti", 'categorie': 'Rappeur et chanteur', 'image': '../static/IMG/playboi-carti.jpg'},
    {'id': 4, 'nomArtiste': "XXX", 'categorie': 'Rappeur et chanteur', 'image': '../static/IMG/xxxtentacion.jpg'},
    {'id': 5, 'nomArtiste': "Lil Uzi Vert", 'categorie': 'Rappeur et chanteur', 'image': '../static/IMG/lil_uzi_vert.jpg'},
    {'id': 6, 'nomArtiste': "Eminem", 'categorie': 'Rappeur et chanteur', 'image': '../static/IMG/The_Eminem_Show.jpg'},
]

#Ca va être le resultat d'une autre requete pour avoir les informations de l'artiste
dataDetail = [
    {'id': 1,'nomArtiste': "Yeat",'description': "Noah Smith (né le 26 février 2000), connu sous le nom de Yeat (stylisé comme YËAT), est un rappeur, chanteur, auteur-compositeur et producteur de musique américain. Il a acquis une reconnaissance à la mi-2021 avec la sortie de sa mixtape '4L' et de son premier album studio 'Up 2 Më'. Des morceaux de ce dernier, dont 'Gët Busy', 'Turban' et 'Monëy So Big', ont gagné en popularité sur TikTok. En 2022, il sort son deuxième album studio, '2 Alivë', ainsi que l'EP 'Lyfë'. Début 2023, il publie son troisième album studio, 'AftërLyfe'.",'image': '../static/IMG/yeat-sans-fond.png'},
    {'id': 2, 'nomArtiste': "Drake", 'description': 'Aubrey Drake Graham, connu professionnellement sous le nom de Drake, est un rappeur, chanteur, auteur-compositeur et producteur de disques canadien.', 'image': '../static/IMG/yeat-sans-fond.jpg'},
    {'id': 3, 'nomArtiste': "Playboi Carti", 'description': 'Jordan Terrell Carter, connu professionnellement sous le nom de Playboi Carti, est un rappeur, chanteur et auteur-compositeur américain.', 'image': '../static/IMG/yeat-sans-fond.jpg'},
    {'id': 4, 'nomArtiste': "XXX", 'description': 'Jahseh Dwayne Ricardo Onfroy, connu sous le nom de XXXTentacion, était un rappeur, chanteur et auteur-compositeur américain.', 'image': '../static/IMG/yeat-sans-fond.jpg'},
    {'id': 5, 'nomArtiste': "Lil Uzi Vert", 'description': 'Symere Woods, connu professionnellement sous le nom de Lil Uzi Vert, est un rappeur, chanteur et compositeur américain.', 'image': '../static/IMG/yeat-sans-fond.jpg'},
    {'id': 6, 'nomArtiste': "Eminem", 'description': 'Marshall Bruce Mathers III, connu professionnellement sous le nom d\'Eminem, est un rappeur, auteur-compositeur et producteur de disques américain.', 'image': '../static/IMG/yeat-sans-fond.jpg'},
]




@app.route("/")
def home():
    user = session.get('user', None)
    adm = session.get('adm',None)
    print(adm)
    print(adm)
    data = [
        {'titreConcert': "Global Journey Travel Packages", 'date': '2022-02-02'},
        {'titreConcert': "Global Journey Travel Packages", 'date': '2022-02-06'},
        {'titreConcert': "Global Journey Travel Packages", 'date': '2022-02-08'},
        {'titreConcert': "Global Journey Travel Packages", 'date': '2022-02-12'}
    ]

    formatted_data = [{'titreConcert': concert['titreConcert'],
                       'date': datetime.strptime(concert['date'], '%Y-%m-%d').strftime('%A %d %B, %Y')}
                      for concert in data]
    return render_template('index.html', data=formatted_data,user=user,adm=adm)

from flask import send_file

@app.route('/image/<int:artiste_id>')
def get_image(artiste_id):
    image_data = mo.get_image_data(artiste_id)
    if image_data:
        return send_file(io.BytesIO(image_data), mimetype='image/jpeg')
    else:
        return "Image non trouvée"


@app.route("/programme/")
def programme_artiste():
    # mo.add_image_to_database(1,mo.image_to_binary('appli/static/IMG/1.jpg'))
    mo.add_all_img()
    user = session.get('user', None)
    return render_template('programme_artiste.html',user=user ,data=mo.Artistes())


def getArtisteById(id):
    for artiste in mo.Artistes():
        if artiste[0] == id:
            return artiste

def getGroupeById(id):
    for groupe in mo.Groupes():
        if groupe[0] == id:
            return groupe

@app.route("/programme/detail-artiste/<int:id>")
def detailArtiste(id):
    user = session.get('user', None)
    artiste = getArtisteById(id)
    return render_template('detail-artiste.html',user=user ,artiste=artiste)

@app.route("/login/")
def login():
    alert_message = request.args.get('alert_message', '')
    return render_template('login.html', alert_message=alert_message)


@app.route("/panier/<int:id>")
def panier(id):
    user = session.get('user', None)
    return render_template('panier.html',user=user,panier=mo.Panier(user[0]),getBilletId=mo.getBilletId)



   

@app.route('/user/')
def user():
    user = session.get('user', None)
    # (1, None, 'Dupont', 'Jean', 'jean.dupont@example.com', 'motdepasse123', datetime.date(1990, 1, 1))
    return render_template('user.html',user=user)

@app.route('/inscription', methods=['POST'])
def inscription():
    """Sauvegarde d'un utilisateur"""
    name = request.form['name']
    mail = request.form['mail']
    mdp = request.form['mdp']
    user = mo.inscription(name, mail, mdp)
    if user is None:
        alert_message = "mail deja inscrit."
        return render_template('login.html', alert_message=alert_message)

    else:
        session['user'] = user
        return redirect(url_for('home'))

@app.route('/connexion', methods=['POST'])
def connexion():
    """Sauvegarde d'un utilisateur"""
    mail = request.form['mail']
    mdp = request.form['mdp']
    user = mo.connexion(mail, mdp)
    print(user)
    if user is None:
        return redirect(url_for('login'))
    else:
        session['user'] = user
        if (user[2] == 'adm1'):
            session['adm'] = False
            print("je passe laaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        return redirect(url_for('home'))
    
@app.route('/deconnexion/')
def deconnexion():
    session.pop('user', None)
    return redirect(url_for('login'))
    
@app.route("/admin/programme")
def admin_artiste():
    
    # mo.add_image_to_database(1,mo.image_to_binary('appli/static/IMG/1.jpg'))
    mo.add_all_img()
    user = session.get('user', None)
    return render_template('admin.html',user=user ,data=mo.Artistes())


@app.route("/admin/suppr_artiste/<int:id>")
def supprimer_artiste(id):
    mo.supprimer_artiste(id) 
    # mo.add_image_to_database(1,mo.image_to_binary('appli/static/IMG/1.jpg'))
    mo.add_all_img()
    user = session.get('user', None)
    artiste = getArtisteById(id)
    return render_template('index.html',user=user ,artiste=artiste)



@app.route("/admin/suppr_user/<int:id>")
def supprimer_user(id):
    mo.supprimer_user(id) 
    user = session.get('user', None)

    return render_template('index.html',user=user )


@app.route('/admin/ajout-artiste', methods=['POST'])
def route_ajout_artiste():
    mo.ajouter_artiste()
    mo.add_all_img()
    user = session.get('user', None)
    artiste = getArtisteById(id)
    return render_template('index.html',user=user ,artiste=artiste)

@app.route('/admin/ajout-groupe', methods=['POST'])
def route_ajout_groupe():
    mo.ajouter_groupe()
    mo.add_all_img()
    user = session.get('user', None)
    groupe = getGroupeById(id)
    return render_template('index.html',user=user ,groupe=groupe)

@app.route("/admin/concert")
def admin_concert():
    
    # mo.add_image_to_database(1,mo.image_to_binary('appli/static/IMG/1.jpg'))
    mo.add_all_img()
    user = session.get('user', None)
    return render_template('adminConcert.html',user=user ,data=mo.Concerts())

@app.route("/admin/user")
def admin_user():
    user = session.get('user', None)
    return render_template('adminUser.html',user=user ,data=mo.Utilisateurs())


@app.route("/billeterie/")
def billeterie():
    user = session.get('user', None)
    return render_template('billeterie.html',user=user)


@app.route('/get_values', methods=['POST'])
def get_values():
    user = session.get('user', None)
    data = request.get_json()
    values = [input_data['value'] for input_data in data]
    mo.ajout_panier(values,user[0])
 
    return jsonify({'values': values})

