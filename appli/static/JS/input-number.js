class InputNumber {
    constructor(containerId) {
        this._value = 0;
        this.containerId = containerId;
        this.max = 10; // Définissez la valeur max ici si nécessaire
        this.min = 0;  // Définissez la valeur min ici si nécessaire
    }

    get value() {
        return this._value;
    }

    set value(newValue) {
        this._value = newValue;
        this.render();
    }

    increment() {
        if (typeof this.max === 'number' && this.value >= this.max) return;
        this.value += 1;
    }
    
    decrement() {
        if (typeof this.min === 'number' && this.value <= this.min) return;
        this.value -= 1;
    }

    input(e) {
        const parseValue = parseInt(e.target.value, 10); // Convertir en nombre
        if (!isNaN(parseValue) && parseValue >= this.min && parseValue <= this.max) {
            this.value = parseValue;
        }
    }
    
    render() {
        const rootElement = document.getElementById(this.containerId);
        rootElement.innerHTML = '';  // Clear previous content
    
        const container = document.createElement('div');
        container.classList.add('input-number');
    
        const decrementButton = this.createButton('-', () => this.decrement());

        // Instead of creating a span, create an input element
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type', 'number');
        inputElement.value = this.value; // Set the value of the input

        const incrementButton = this.createButton('+', () => this.increment());
    
        container.appendChild(decrementButton);
        container.appendChild(inputElement); // Add the input element to the container
        container.appendChild(incrementButton);
    
        rootElement.appendChild(container);
    }

    createButton(text, onClick) {
        const button = document.createElement('button');
        button.setAttribute('type', 'button');
        button.textContent = text;
        button.addEventListener('click', onClick);
        return button;
    }
}

const inputNumbers = [];

for (let i = 1; i <= 5; i++) {
    const inputNumber = new InputNumber(`root${i}`);
    inputNumbers.push(inputNumber);
}

document.addEventListener('DOMContentLoaded', () => {
    inputNumbers.forEach(inputNumber => inputNumber.render());
});
