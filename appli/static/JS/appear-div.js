function toggleVisibility(billetConcertDateClass, chevronIconId) {
  var billetConcertDate = document.querySelector("." + billetConcertDateClass);
  var chevronIcon = document.getElementById(chevronIconId);

  // Basculer la classe 'visible' de la div billet-concert-date
  billetConcertDate.classList.toggle("visible");

  // Basculer la classe 'fa-chevron-up' et 'fa-chevron-down' pour l'icône
  chevronIcon.classList.toggle("fa-chevron-up");
  chevronIcon.classList.toggle("fa-chevron-down");
}


