class Panier {
    constructor() {
        this.articles = [];
    }

    ajouterArticle(article) {
        this.articles.push(article);
    }

    retirerArticle(article) {
        const index = this.articles.indexOf(article);
        if (index !== -1) {
            this.articles.splice(index, 1);
        }
    }

    viderPanier() {
        this.articles = [];
    }

    calculerTotal() {
        return this.articles.reduce((total, article) => total + article.prix * article.quantite, 0);
    }
}
