from flask import redirect, render_template, session, url_for
import mysql.connector
from PIL import Image
import io
from io import BytesIO
import datetime
import json
from .app import app
 
# db = mysql.connector.connect(host="servinfo-mariadb",
#                              user="menjikoff",
#                              password="menjikoff",
#                              database="DBmenjikoff")

db = mysql.connector.connect(host="localhost",
                             user="root",
                             password="",
                             database="DBmenjikoff")




def get_cursor():
    return db.cursor()

def close_cursor(cursor):
    cursor.close()

def execute_query(cursor, query, params=None):
    """Fonction permettant d'executer une requete SQL

    Args:
        cursor (): _description_
        query (_type_): _description_
        params (_type_, optional): _description_. Defaults to None.
    """
    if params:
        cursor.execute(query, params)
    else:
        cursor.execute(query)


def Artistes():
    """Fonction permettant de récupérer les prochains concerts

    Returns:
        list: Les prochains concerts
    """
    try:
        cursor = get_cursor()
        req1 = (
            "SELECT * FROM ARTISTES "
        )
        execute_query(cursor, req1)
        infos = cursor.fetchall()
        close_cursor(cursor)
        return infos
    except Exception as e:
        print(e.args)
        return []
    
def Concerts():
    """Fonction permettant de récupérer les prochains concerts

    Returns:
        list: Les prochains concerts
    """
    try:
        cursor = get_cursor()
        req1 = (
            "SELECT * FROM CONCERTS "
        )
        execute_query(cursor, req1)
        infos = cursor.fetchall()
        close_cursor(cursor)
        return infos
    except Exception as e:
        print(e.args)
        return []
    

 
def Utilisateurs():
    """Fonction permettant de récupérer les prochains concerts

    Returns:
        list: Les prochains concerts
    """
    try:
        cursor = get_cursor()
        req1 = (
            "SELECT * FROM UTILISATEURS "
        )
        execute_query(cursor, req1)
        infos = cursor.fetchall()
        close_cursor(cursor)
        return infos
    except Exception as e:
        print(e.args)
        return []
    
def Groupes():
    """Fonction permettant de récupérer les prochains groupes

    Returns:
        list: Les prochains groupes
    """
    try:
        cursor = get_cursor()
        req1 = (
            "SELECT * FROM GROUPES "
        )
        execute_query(cursor, req1)
        infos = cursor.fetchall()
        close_cursor(cursor)
        return infos
    except Exception as e:
        print(e.args)
        return []
    
def getMaxId():
    """Fonction permettant de récupérer les prochains concerts

    Returns:
        list: Les prochains concerts
    """
    try:
        cursor = get_cursor()
        req1 = (
            "select max(utilisateur_id) from UTILISATEURS"
        )
        execute_query(cursor, req1)
        id = cursor.fetchone()
        close_cursor(cursor)
        return id[0]
    except Exception as e:
        print(e.args)
        return None


def inscription(name, mail, mdp):
    try:
        cursor = get_cursor()

        # Vérifier si l'email existe déjà dans la base de données
        check_email_query = "SELECT utilisateur_id FROM UTILISATEURS WHERE email = %s"
        cursor.execute(check_email_query, (mail,))
        existing_user = cursor.fetchone()

        if existing_user:
            close_cursor(cursor)
            return None
        else:
            # L'email n'existe pas, procéder à l'insertion
            insert_query = "INSERT INTO UTILISATEURS (utilisateur_id, billet_id, nomUser, prenomUser, email, mot_de_passe, date_naissance) VALUES(%s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(insert_query, (getMaxId() + 1, None, name, None, mail, mdp, None))

            insert_query = "INSERT INTO PANIER (panier_id,utilisateur_id) VALUES(%s, %s)"
            cursor.execute(insert_query, (getMaxId() ,getMaxId() ))
            db.commit()
            get_user_query = "SELECT * FROM UTILISATEURS WHERE email = %s"
            cursor.execute(get_user_query, (mail,))
            user = cursor.fetchone()
            close_cursor(cursor)
            return user
        
    except Exception as e:
        print(e.args)

def connexion(mail, mdp):
    try:
        cursor = get_cursor()
        req1 = (
            "select * from UTILISATEURS where email=%s and mot_de_passe=%s"
        )
        cursor.execute(req1,(mail,mdp))
        id = cursor.fetchone()
        close_cursor(cursor)
        return id
    except Exception as e:
        print(e.args)
        return None
    

def add_image_to_database(artiste_id, image_data):
    cursor = None
    try:
        db = mysql.connector.connect(host="localhost",
                                        user="root",
                                        password="",
                                        database="DBmenjikoff")

        cursor = db.cursor()

        # Vérifier si l'artiste existe déjà
        select_query = "SELECT * FROM ARTISTES WHERE artiste_id = %s"
        cursor.execute(select_query, (artiste_id,))
        existing_artist = cursor.fetchone()

        if existing_artist:
            # L'artiste existe, effectuer une mise à jour de l'image
            update_query = "UPDATE ARTISTES SET image_data = %s WHERE artiste_id = %s"
            cursor.execute(update_query, (image_data, artiste_id))
        else:
            # L'artiste n'existe pas, effectuer une insertion
            insert_query = "INSERT INTO ARTISTES (artiste_id, image_data) VALUES (%s, %s)"
            cursor.execute(insert_query, (artiste_id, image_data))

        # Commit des modifications
        db.commit()

        print(f"Image ajoutée avec succès pour l'artiste ID {artiste_id}")

    except Exception as e:
        print(f"Erreur lors de l'ajout de l'image : {e}")

    finally:
        if cursor:
            cursor.close()
        db.close()


def add_all_img():
    try:
        cursor = get_cursor()
        req1 = (
            "SELECT * FROM ARTISTES "
        )
        execute_query(cursor, req1)
        arts = cursor.fetchall()
        close_cursor(cursor)
        for id in range(1, len(arts) + 1):
            print(id)
            chemin = 'appli/static/IMG/' + str(id) + '.jpg'
            print(chemin)
            add_image_to_database(id,image_to_binary(chemin))
    except Exception as e:
        print(e.args)


def image_to_binary(image_path):
    try:
        # Ouvrir l'image avec PIL
        with open(image_path, 'rb') as image_file:
            image = Image.open(image_file)

            # Convertir l'image en mode RVB (si elle n'est pas déjà dans ce mode)
            image = image.convert("RGB")

            # Créer un objet BytesIO pour stocker les données binaires
            image_bytesio = io.BytesIO()

            # Enregistrer l'image dans l'objet BytesIO
            image.save(image_bytesio, format='JPEG')

            # Récupérer les données binaires de l'objet BytesIO
            image_data = image_bytesio.getvalue()

            return image_data

    except Exception as e:
        print(f"Erreur lors de la conversion de l'image en données binaires : {e}")
        return None

def get_image_data(artiste_id):
    cursor = None
    try:
        db = mysql.connector.connect(host="localhost",
                                user="root",
                                password="",
                                database="DBmenjikoff")

        cursor = db.cursor()

        # Récupérer les données binaires de l'image
        select_query = "SELECT image_data FROM ARTISTES WHERE artiste_id = %s"
        cursor.execute(select_query, (artiste_id,))
        image_data = cursor.fetchone()

        return image_data[0] if image_data else None

    except Exception as e:
        print(f"Erreur lors de la récupération de l'image : {e}")
        return None

    finally:
        # Fermeture du curseur et de la connexion à la base de données
        if cursor:
            cursor.close()
        if db.is_connected():
            db.close()


def supprimer_artiste(id):
    cursor = get_cursor()
    
    
        # Delete records from the liens_reseaux table referencing the specified artist_id
    delete_membres_groupes_query = "DELETE FROM membres_groupes WHERE artiste_id = %s"
    cursor.execute(delete_membres_groupes_query, (id,))
    
        # Delete records from the liens_reseaux table referencing the specified artist_id
    delete_favoris_spectateurs_query = "DELETE FROM favoris_spectateurs WHERE artiste_id = %s"
    cursor.execute(delete_favoris_spectateurs_query, (id,))
    
    
        # Delete records from the liens_reseaux table referencing the specified artist_id
    delete_photos_query = "DELETE FROM photos WHERE artiste_id = %s"
    cursor.execute(delete_photos_query, (id,))
    
        # Delete records from the liens_reseaux table referencing the specified artist_id
    delete_liens_videos_query = "DELETE FROM liens_videos WHERE artiste_id = %s"
    cursor.execute(delete_liens_videos_query, (id,))
    
    # Delete records from the liens_reseaux table referencing the specified artist_id
    delete_liens_reseaux_query = "DELETE FROM liens_reseaux WHERE artiste_id = %s"
    cursor.execute(delete_liens_reseaux_query, (id,))
    
    
    # Delete from the "CONCERTS" table
    delete_concerts_query = "DELETE FROM CONCERTS WHERE artiste_id = %s"
    cursor.execute(delete_concerts_query, (id,))

    # Delete from the "ARTISTES" table
    delete_artist_query = "DELETE FROM ARTISTES WHERE artiste_id = %s"
    cursor.execute(delete_artist_query, (id,))
    
    # Commit des modifications
    db.commit()

    close_cursor(cursor)

    return "Artiste supprimé"

def supprimer_user(id):
    cursor = get_cursor()
    delete_user_query = "DELETE FROM UTILISATEURS WHERE utilisateur_id = %s"
    cursor.execute(delete_user_query, (id,))
    db.commit()

    close_cursor(cursor)

    return "user supprimé"


from flask import request

def ajouter_artiste():
    if request.method == 'POST':
        try:
            cursor = get_cursor()

            # Récupérer les données du formulaire
            nom_artiste = request.form['nomArtiste']
            desc_artiste = request.form['desc_artiste']
            categorie = request.form['categorie']
            style_musical_id = int(request.form['style_musical_id'])
            date_heure_arrivee = request.form['date_heure_arrivee']
            date_heure_depart = request.form['date_heure_depart']
            instrument_joue = request.form['instrument_joue']

            # Insérer l'artiste dans la table ARTISTES
            insert_query = "INSERT INTO ARTISTES (nomArtiste, desc_artiste, categorie, style_musical_id, date_heure_arrivee, date_heure_depart, instrument_joue) VALUES (%s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(insert_query, (nom_artiste, desc_artiste, categorie, style_musical_id, date_heure_arrivee, date_heure_depart, instrument_joue))
            
            # Commit des modifications
            db.commit()

            close_cursor(cursor)

            # Récupérer l'ID de l'artiste nouvellement inséré
            select_last_id_query = "SELECT LAST_INSERT_ID()"
            cursor.execute(select_last_id_query)
            artiste_id = cursor.fetchone()[0]

            # Ajouter l'image à la base de données
            image = request.files['image']
            if image:
                image_data = image.read()
                add_image_to_database(artiste_id, image_data)

            return redirect(url_for('appli/static/IMG/'))

        except Exception as e:
            print(f"Erreur lors de l'ajout de l'artiste : {e}")
            db.rollback()
            close_cursor(cursor)

    # Rediriger vers une page ou renvoyer un message d'erreur si la méthode n'est pas POST
    return redirect(url_for('programme_artiste'))


def ajouter_groupe():
    if request.method == 'POST':
        try:
            cursor = get_cursor()

            # Récupérer les données du formulaire
            nom_groupe = request.form['nomGroupe']
            desc_groupe = request.form['desc_groupe']
            style_musical_id = int(request.form['style_musical_id'])
            date_heure_arrivee = request.form['date_heure_arrivee']
            date_heure_depart = request.form['date_heure_depart']

            # Insérer le groupe dans la table GROUPES
            insert_query = "INSERT INTO GROUPES (nomGroupe, desc_groupe, style_musical_id, date_heure_arrivee, date_heure_depart) VALUES (%s, %s, %s, %s, %s)"
            cursor.execute(insert_query, (nom_groupe, desc_groupe, style_musical_id, date_heure_arrivee, date_heure_depart))
            
            # Commit des modifications
            db.commit()

            close_cursor(cursor)

            # Récupérer l'ID du groupe nouvellement inséré
            select_last_id_query = "SELECT LAST_INSERT_ID()"
            cursor.execute(select_last_id_query)
            groupe_id = cursor.fetchone()[0]

            # Ajouter d'autres informations spécifiques au groupe si nécessaire

            return redirect(url_for('programme_artiste'))  # Mettez le nom de votre route correct ici

        except Exception as e:
            print(f"Erreur lors de l'ajout du groupe : {e}")
            db.rollback()
            close_cursor(cursor)

    # Rediriger vers une page ou renvoyer un message d'erreur si la méthode n'est pas POST
    return redirect(url_for('programme_artiste'))  # Mettez le nom de votre route correct ici

def ajout_panier(billets, iduser):
    for i in range(len(billets)):
        if billets[i] != 0:
            try:
                db = mysql.connector.connect(host="localhost",
                                user="root",
                                password="",
                                database="DBmenjikoff")
                cursor = db.cursor()  # Corrected line
                insert_query = "INSERT INTO PANIER_BILLETS (panier_id, billet_id, quantite_achetee) VALUES (%s, %s, %s)"
                cursor.execute(insert_query, (iduser, i+1, billets[i]))
                db.commit()
            except Exception as e:
                print(f"Erreur lors de l'insertion : {e}")
                return None


    
def Panier(id):
    """Fonction permettant de récupérer les prochains concerts

    Returns:
        list: Les prochains concerts
    """
    try:
        cursor = get_cursor()
        select_query =  "SELECT * FROM PANIER_BILLETS WHERE panier_id=%s "
        
        
        cursor.execute(select_query, (id,))
        infos = cursor.fetchall()
        close_cursor(cursor)
        return infos
    except Exception as e:
        print(e.args)
        return []
    
def getBilletId(id):
    try:
        cursor = get_cursor()
        select_query =  "SELECT * FROM BILLETS WHERE billet_id=%s "
        
        
        cursor.execute(select_query, (id,))
        infos = cursor.fetchone()
        close_cursor(cursor)
        return infos
    except Exception as e:
        print(e.args)
        return []

